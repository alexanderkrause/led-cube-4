#ifndef LIGHTING_H
#define LIGHTING_H

#include "LightingState.h"

/*
* Represents State machine
*/

class Lighting
{
private:
    /* data */
    LightingState* _state;
public:
    Lighting(/* args */);
    ~Lighting();
    //void setLighting(LightingState* state, uint32_t argument);
    void setLighting(LightingState* state);
    void shine(uint32_t argument);
    void shine();
    void resetLighting();
};


#endif