#include <Arduino.h>
#include "Lighting.h"
#include "LightingState.h"
#include "LightingStates/Full.h"
#include "LightingStates/Sweep.h"
#include "LightingStates/RandomDot.h"

#ifdef debug
    void testMethod();
#endif

#define LED0   52 
#define LED1   50
#define LED2   48
#define LED3   46
#define LED4   44
#define LED5   42
#define LED6   40
#define LED7   38
#define LED8   36
#define LED9   34
#define LED10  32
#define LED11  30
#define LED12  28
#define LED13  26
#define LED14  24
#define LED15  22

#define LAYER0 47
#define LAYER1 49
#define LAYER2 51
#define LAYER3 53


/*
Anschlüsse links
Ansicht von oben
Layer 0 unten, Layer 3 oben

Y 
3 |  12  13  14  15
2 |   8   9  10  11
1 |   4   5   6   7
0 |   0   1   2   3
  |_________________
X     0   1   2   3

*/

uint8_t LED[16] = {LED0, LED1, LED2, LED3, LED4, LED5, LED6, LED7, LED8, LED9, LED10, LED11, LED12, LED13, LED14, LED15};
uint8_t LAYER[4] = {LAYER0, LAYER1, LAYER2, LAYER3};

LightingState* full;
LightingState* sweep;
LightingState* randomDot;
Lighting* stateMachine;

void setup() {
    // put your setup code here, to run once:

    //Initialize serial and wait for port to open:
    Serial.begin(9600);
    while (!Serial) {
        // wait for serial port to connect. Needed for native USB port only
    }

    #ifdef debug
        Serial.println("Debug session initiated...");
    #endif

    
    for (int8_t i = 0; i < 16; ++i)
    {
        pinMode(LED[i], OUTPUT);
        digitalWrite(LED[i], LOW);
    }

    for (int8_t i = 0; i < 4; ++i)
    {
        pinMode(LAYER[i], OUTPUT);
        digitalWrite(LAYER[i], HIGH);
    }
    
    
    for (int8_t i = 0; i < 4; ++i)
    {
        digitalWrite(LAYER[i], LOW);
        for (int8_t j = 0; j < 16; ++j)
        {
            digitalWrite(LED[j], HIGH);
            delay(50);
            digitalWrite(LED[j], LOW);
        }
        digitalWrite(LAYER[i], HIGH);
    }
    
    randomSeed(analogRead(0));

    full = new Full(LED, LAYER);
    sweep = new Sweep(LED, LAYER);
    randomDot = new RandomDot(LED, LAYER);
    stateMachine = new Lighting();

    stateMachine->resetLighting();

    stateMachine->setLighting(full);

    #ifdef debug
        testMethod();
    #endif
}

void loop() {
    // put your main code here, to run repeatedly:
    #ifdef debug
        Serial.println("New loop iteration...");
    #endif

    stateMachine->setLighting(randomDot);
    stateMachine->shine(500);
    stateMachine->shine(500);
    stateMachine->shine(500);
    stateMachine->shine(500);
    stateMachine->shine(500);


    stateMachine->setLighting(full);
    stateMachine->shine();
    stateMachine->setLighting(sweep);
    stateMachine->shine(0);
    stateMachine->shine(1); //
    stateMachine->shine(2);
    stateMachine->shine(3); //
    stateMachine->shine(4);
    stateMachine->shine(5);
}

#ifdef debug
void testMethod() 
{
    Serial.println("TestMethod");

    stateMachine->setLighting(full);

    while (true)
    {
        full->illuminate();
    }
    
}
#endif