#include "Full.h"
#include <Arduino.h>

void Full::shine()
{   
    shine(1000);
}

void Full::shine(uint32_t argument)
{
    unsigned long start = millis();
    while ((millis() - start) < argument)
    {
        illuminate();
    }
}

Full::Full(uint8_t* LED, uint8_t* LAYER)
{
    this->LED = LED;
    this->LAYER = LAYER;

    uint8_t a000[3] = {0, 0, 0};
    uint8_t a001[3] = {0, 0, 1};
    uint8_t a002[3] = {0, 0, 2};
    uint8_t a003[3] = {0, 0, 3};

    uint8_t a010[3] = {0, 1, 0};
    uint8_t a011[3] = {0, 1, 1};
    uint8_t a012[3] = {0, 1, 2};
    uint8_t a013[3] = {0, 1, 3};

    uint8_t a020[3] = {0, 2, 0};
    uint8_t a021[3] = {0, 2, 1};
    uint8_t a022[3] = {0, 2, 2};
    uint8_t a023[3] = {0, 2, 3};

    uint8_t a030[3] = {0, 3, 0};
    uint8_t a031[3] = {0, 3, 1};
    uint8_t a032[3] = {0, 3, 2};
    uint8_t a033[3] = {0, 3, 3};


    uint8_t a100[3] = {1, 0, 0};
    uint8_t a101[3] = {1, 0, 1};
    uint8_t a102[3] = {1, 0, 2};
    uint8_t a103[3] = {1, 0, 3};

    uint8_t a110[3] = {1, 1, 0};
    uint8_t a111[3] = {1, 1, 1};
    uint8_t a112[3] = {1, 1, 2};
    uint8_t a113[3] = {1, 1, 3};

    uint8_t a120[3] = {1, 2, 0};
    uint8_t a121[3] = {1, 2, 1};
    uint8_t a122[3] = {1, 2, 2};
    uint8_t a123[3] = {1, 2, 3};

    uint8_t a130[3] = {1, 3, 0};
    uint8_t a131[3] = {1, 3, 1};
    uint8_t a132[3] = {1, 3, 2};
    uint8_t a133[3] = {1, 3, 3};


    uint8_t a200[3] = {2, 0, 0};
    uint8_t a201[3] = {2, 0, 1};
    uint8_t a202[3] = {2, 0, 2};
    uint8_t a203[3] = {2, 0, 3};

    uint8_t a210[3] = {2, 1, 0};
    uint8_t a211[3] = {2, 1, 1};
    uint8_t a212[3] = {2, 1, 2};
    uint8_t a213[3] = {2, 1, 3};

    uint8_t a220[3] = {2, 2, 0};
    uint8_t a221[3] = {2, 2, 1};
    uint8_t a222[3] = {2, 2, 2};
    uint8_t a223[3] = {2, 2, 3};

    uint8_t a230[3] = {2, 3, 0};
    uint8_t a231[3] = {2, 3, 1};
    uint8_t a232[3] = {2, 3, 2};
    uint8_t a233[3] = {2, 3, 3};


    uint8_t a300[3] = {3, 0, 0};
    uint8_t a301[3] = {3, 0, 1};
    uint8_t a302[3] = {3, 0, 2};
    uint8_t a303[3] = {3, 0, 3};

    uint8_t a310[3] = {3, 1, 0};
    uint8_t a311[3] = {3, 1, 1};
    uint8_t a312[3] = {3, 1, 2};
    uint8_t a313[3] = {3, 1, 3};

    uint8_t a320[3] = {3, 2, 0};
    uint8_t a321[3] = {3, 2, 1};
    uint8_t a322[3] = {3, 2, 2};
    uint8_t a323[3] = {3, 2, 3};

    uint8_t a330[3] = {3, 3, 0};
    uint8_t a331[3] = {3, 3, 1};
    uint8_t a332[3] = {3, 3, 2};
    uint8_t a333[3] = {3, 3, 3};

    uint8_t* points[64] = { a000, a001, a002, a003, a010, a011, a012, a013, a020, a021, a022, a023, a030, a031, a032, a033,
                            a100, a101, a102, a103, a110, a111, a112, a113, a120, a121, a122, a123, a130, a131, a132, a133,
                            a200, a201, a202, a203, a210, a211, a212, a213, a220, a221, a222, a223, a230, a231, a232, a233,
                            a300, a301, a302, a303, a310, a311, a312, a313, a320, a321, a322, a323, a330, a331, a332, a333};
    
    transformedData = transform(points, 64, 1000);
    this->setLightingSequence(&transformedData, 1);
    
    #ifdef debug
        Serial.println("Full created!");
    #endif
}

Full::~Full()
{
    #ifdef debug
        Serial.println("Full destroyed!");
    #endif 
}

String Full::toString()
{
    return "Full";
}