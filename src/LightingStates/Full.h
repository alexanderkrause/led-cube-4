#ifndef FULL_H
#define FULL_H

#include "LightingState.h"
#include <Arduino.h>

class Full : public LightingState
{
private:
    /* data */
    LightingState::LightingSequencePoint transformedData;
    
public:
    void shine(uint32_t argument);
    void shine();
    String toString();
    Full(uint8_t* LED, uint8_t* LAYER);
    ~Full();
};

#endif