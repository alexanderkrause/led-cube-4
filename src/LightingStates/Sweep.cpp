#include "Sweep.h"
#include <Arduino.h>

void Sweep::shine()
{   
    shine(0);
}

void Sweep::shine(uint32_t argument)
{
    switch (argument)
    {
    case 0:     // right
        setLightingSequence(left);
        break;
    case 1:     // left
        setLightingSequence(right);
        break;
    case 2:     // back
        setLightingSequence(front);
        break;
    case 3:     // front
        setLightingSequence(back);
        break;
    case 4:     // up
        setLightingSequence(up);
        break;
    case 5:     // down
        setLightingSequence(down);
        break;
    default:
        break;
    }

    illuminate();
}

Sweep::Sweep(uint8_t* LED, uint8_t* LAYER)
{
    this->LED = LED;
    this->LAYER = LAYER;

    uint8_t a000[3] = {0, 0, 0};
    uint8_t a001[3] = {0, 0, 1};
    uint8_t a002[3] = {0, 0, 2};
    uint8_t a003[3] = {0, 0, 3};

    uint8_t a010[3] = {0, 1, 0};
    uint8_t a011[3] = {0, 1, 1};
    uint8_t a012[3] = {0, 1, 2};
    uint8_t a013[3] = {0, 1, 3};

    uint8_t a020[3] = {0, 2, 0};
    uint8_t a021[3] = {0, 2, 1};
    uint8_t a022[3] = {0, 2, 2};
    uint8_t a023[3] = {0, 2, 3};

    uint8_t a030[3] = {0, 3, 0};
    uint8_t a031[3] = {0, 3, 1};
    uint8_t a032[3] = {0, 3, 2};
    uint8_t a033[3] = {0, 3, 3};


    uint8_t a100[3] = {1, 0, 0};
    uint8_t a101[3] = {1, 0, 1};
    uint8_t a102[3] = {1, 0, 2};
    uint8_t a103[3] = {1, 0, 3};

    uint8_t a110[3] = {1, 1, 0};
    uint8_t a111[3] = {1, 1, 1};
    uint8_t a112[3] = {1, 1, 2};
    uint8_t a113[3] = {1, 1, 3};

    uint8_t a120[3] = {1, 2, 0};
    uint8_t a121[3] = {1, 2, 1};
    uint8_t a122[3] = {1, 2, 2};
    uint8_t a123[3] = {1, 2, 3};

    uint8_t a130[3] = {1, 3, 0};
    uint8_t a131[3] = {1, 3, 1};
    uint8_t a132[3] = {1, 3, 2};
    uint8_t a133[3] = {1, 3, 3};


    uint8_t a200[3] = {2, 0, 0};
    uint8_t a201[3] = {2, 0, 1};
    uint8_t a202[3] = {2, 0, 2};
    uint8_t a203[3] = {2, 0, 3};

    uint8_t a210[3] = {2, 1, 0};
    uint8_t a211[3] = {2, 1, 1};
    uint8_t a212[3] = {2, 1, 2};
    uint8_t a213[3] = {2, 1, 3};

    uint8_t a220[3] = {2, 2, 0};
    uint8_t a221[3] = {2, 2, 1};
    uint8_t a222[3] = {2, 2, 2};
    uint8_t a223[3] = {2, 2, 3};

    uint8_t a230[3] = {2, 3, 0};
    uint8_t a231[3] = {2, 3, 1};
    uint8_t a232[3] = {2, 3, 2};
    uint8_t a233[3] = {2, 3, 3};


    uint8_t a300[3] = {3, 0, 0};
    uint8_t a301[3] = {3, 0, 1};
    uint8_t a302[3] = {3, 0, 2};
    uint8_t a303[3] = {3, 0, 3};

    uint8_t a310[3] = {3, 1, 0};
    uint8_t a311[3] = {3, 1, 1};
    uint8_t a312[3] = {3, 1, 2};
    uint8_t a313[3] = {3, 1, 3};

    uint8_t a320[3] = {3, 2, 0};
    uint8_t a321[3] = {3, 2, 1};
    uint8_t a322[3] = {3, 2, 2};
    uint8_t a323[3] = {3, 2, 3};

    uint8_t a330[3] = {3, 3, 0};
    uint8_t a331[3] = {3, 3, 1};
    uint8_t a332[3] = {3, 3, 2};
    uint8_t a333[3] = {3, 3, 3};

    uint8_t* layerX0[16] = { a000, a001, a002, a003, a010, a011, a012, a013, a020, a021, a022, a023, a030, a031, a032, a033};
    uint8_t* layerX1[16] = { a100, a101, a102, a103, a110, a111, a112, a113, a120, a121, a122, a123, a130, a131, a132, a133};
    uint8_t* layerX2[16] = { a200, a201, a202, a203, a210, a211, a212, a213, a220, a221, a222, a223, a230, a231, a232, a233};
    uint8_t* layerX3[16] = { a300, a301, a302, a303, a310, a311, a312, a313, a320, a321, a322, a323, a330, a331, a332, a333};

    uint8_t* layerY0[16] = { a000, a001, a002, a003, a100, a101, a102, a103, a200, a201, a202, a203, a300, a301, a302, a303};
    uint8_t* layerY1[16] = { a010, a011, a012, a013, a110, a111, a112, a113, a210, a211, a212, a213, a310, a311, a312, a313};
    uint8_t* layerY2[16] = { a020, a021, a022, a023, a120, a121, a122, a123, a220, a221, a222, a223, a320, a321, a322, a323};
    uint8_t* layerY3[16] = { a030, a031, a032, a033, a130, a131, a132, a133, a230, a231, a232, a233, a330, a331, a332, a333};

    uint8_t* layerZ0[16] = { a000, a010, a020, a030, a100, a110, a120, a130, a200, a210, a220, a230, a300, a310, a320, a330};
    uint8_t* layerZ1[16] = { a001, a011, a021, a031, a101, a111, a121, a131, a201, a211, a221, a231, a301, a311, a321, a331};
    uint8_t* layerZ2[16] = { a002, a012, a022, a032, a102, a112, a122, a132, a202, a212, a222, a232, a302, a312, a322, a332};
    uint8_t* layerZ3[16] = { a003, a013, a023, a033, a103, a113, a123, a133, a203, a213, a223, a233, a303, a313, a323, a333};


    transformedDataX0 = transform(layerX0, 16, 150);
    transformedDataX1 = transform(layerX1, 16, 150);
    transformedDataX2 = transform(layerX2, 16, 150);
    transformedDataX3 = transform(layerX3, 16, 150);

    transformedDataY0 = transform(layerY0, 16, 150);
    transformedDataY1 = transform(layerY1, 16, 150);
    transformedDataY2 = transform(layerY2, 16, 150);
    transformedDataY3 = transform(layerY3, 16, 150);

    transformedDataZ0 = transform(layerZ0, 16, 150);
    transformedDataZ1 = transform(layerZ1, 16, 150);
    transformedDataZ2 = transform(layerZ2, 16, 150);
    transformedDataZ3 = transform(layerZ3, 16, 150);

    LightingState::LightingSequencePoint b0[4] = {transformedDataX0, transformedDataX1, transformedDataX2, transformedDataX3};
    setLightingSequence(b0, 4);
    right = getLightingSequence();

    LightingState::LightingSequencePoint b1[4] = {transformedDataX3, transformedDataX2, transformedDataX1, transformedDataX0};
    setLightingSequence(b1, 4);
    left = getLightingSequence();

    LightingState::LightingSequencePoint b2[4] = {transformedDataY0, transformedDataY1, transformedDataY2, transformedDataY3};
    setLightingSequence(b2, 4);
    back = getLightingSequence();

    LightingState::LightingSequencePoint b3[4] = {transformedDataY3, transformedDataY2, transformedDataY1, transformedDataY0};
    setLightingSequence(b3, 4);
    front = getLightingSequence();

    LightingState::LightingSequencePoint b4[4] = {transformedDataZ0, transformedDataZ1, transformedDataZ2, transformedDataZ3};
    setLightingSequence(b4, 4);
    up = getLightingSequence();

    LightingState::LightingSequencePoint b5[4] = {transformedDataZ3, transformedDataZ2, transformedDataZ1, transformedDataZ0};
    setLightingSequence(b5, 4);
    down = getLightingSequence();
    
    #ifdef debug
        Serial.println("Sweep created!");
    #endif
}

Sweep::~Sweep()
{
    #ifdef debug
        Serial.println("Sweep destroyed!");
    #endif 
}

String Sweep::toString()
{
    return "Sweep";
}