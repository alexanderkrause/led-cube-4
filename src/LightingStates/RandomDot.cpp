#include "RandomDot.h"
#include <Arduino.h>

void RandomDot::shine()
{   
    shine(1000);
}

void RandomDot::shine(uint32_t argument)
{
    long rand = random(0, 64);
    uint8_t* dot[3];
    *dot = points[rand];
    transformedData = transform(dot, 1, 1000);
    this->setLightingSequence(&transformedData, 1);

    unsigned long start = millis();
    while ((millis() - start) < argument)
    {
        illuminate();
    }
}

RandomDot::RandomDot(uint8_t* LED, uint8_t* LAYER)
{
    this->LED = LED;
    this->LAYER = LAYER;
    
    #ifdef debug
        Serial.println("RandomDot created!");
    #endif
}

RandomDot::~RandomDot()
{
    #ifdef debug
        Serial.println("RandomDot destroyed!");
    #endif 
}

String RandomDot::toString()
{
    return "RandomDot";
}