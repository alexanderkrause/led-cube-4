#ifndef CLEAR_H
#define CLEAR_H

#include "LightingState.h"
#include <Arduino.h>

class Clear : public LightingState
{
private:
    /* data */
    
public:
    void shine(uint32_t argument);
    void shine();
    String toString();
    Clear(uint8_t* LED, uint8_t* LAYER);
    ~Clear();
};

#endif