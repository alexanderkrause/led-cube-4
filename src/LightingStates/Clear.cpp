#include "Clear.h"
#include <Arduino.h>

void Clear::shine()
{   
    shine(1000);
}

void Clear::shine(uint32_t argument)
{
    unsigned long start = millis();
    while ((millis() - start) < argument)
    {
        resetLighting();
    }
}

Clear::Clear(uint8_t* LED, uint8_t* LAYER)
{
    this->LED = LED;
    this->LAYER = LAYER;
    
    #ifdef debug
        Serial.println("Clear created!");
    #endif
}

Clear::~Clear()
{
    #ifdef debug
        Serial.println("Clear destroyed!");
    #endif 
}

String Clear::toString()
{
    return "Clear";
}