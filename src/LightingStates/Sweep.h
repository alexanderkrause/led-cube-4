#ifndef SWEEP_H
#define SWEEP_H

#include "LightingState.h"
#include <Arduino.h>

class Sweep : public LightingState
{
private:
    /* data */
    LightingState::LightingSequencePoint transformedDataX0;
    LightingState::LightingSequencePoint transformedDataX1;
    LightingState::LightingSequencePoint transformedDataX2;
    LightingState::LightingSequencePoint transformedDataX3;

    LightingState::LightingSequencePoint transformedDataY0;
    LightingState::LightingSequencePoint transformedDataY1;
    LightingState::LightingSequencePoint transformedDataY2;
    LightingState::LightingSequencePoint transformedDataY3;

    LightingState::LightingSequencePoint transformedDataZ0;
    LightingState::LightingSequencePoint transformedDataZ1;
    LightingState::LightingSequencePoint transformedDataZ2;
    LightingState::LightingSequencePoint transformedDataZ3;

    LightingState::LightingSequence* up;
    LightingState::LightingSequence* down;
    LightingState::LightingSequence* left;
    LightingState::LightingSequence* right;
    LightingState::LightingSequence* front;
    LightingState::LightingSequence* back;

    
public:
    void shine(uint32_t argument);
    void shine();
    String toString();
    Sweep(uint8_t* LED, uint8_t* LAYER);
    ~Sweep();
};

#endif