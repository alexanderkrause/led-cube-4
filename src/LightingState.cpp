#include "LightingState.h"

LightingState::LightingSequencePoint LightingState::transform(uint8_t* points[], uint8_t length, uint16_t duration)
{
    LightingPointArrayRepresentation* pointArray = new LightingPointArrayRepresentation[length];
    LightingSequencePoint sequencePoint;
    
    sequencePoint.duration = duration;
    #ifdef debug
        Serial.print("Going to set length to: ");
        Serial.println(length);
    #endif
    sequencePoint.length = length;
    #ifdef debug
        Serial.print("Set length to: ");
        Serial.println(sequencePoint.length);
    #endif

    for (uint8_t i = 0; i < length; ++i)
    {

        uint8_t column = points[i][0] + 4 * points[i][1];
        uint8_t layer = points[i][2];
        
        pointArray[i].LED = this->LED[column];
        pointArray[i].LAYER = this->LAYER[layer];
        #ifdef debug
            Serial.println("***TRANSFORM SETTING***");
            Serial.println(column);
            Serial.println(layer);
            Serial.println(points[i][0]);
            Serial.println(points[i][1]);
            Serial.println(points[i][2]);
            Serial.println(this->LED[column]);
            Serial.println(pointArray[i].LED);
            Serial.println(this->LAYER[layer]);
            Serial.println(pointArray[i].LAYER);
        #endif
    }

    sequencePoint.lightingPointArrayRepresentation = pointArray;

    return sequencePoint;
}

void LightingState::illuminate()
{
    #ifdef debug
        Serial.println("illuminate()");
    #endif
    for (uint8_t i = 0; i < lightingSequence->length; ++i)
    {   
        #ifdef debug
            Serial.println("Begin Loop 1 Iteration Start");
        #endif

        unsigned long start = millis();
        while ((millis() - start) < lightingSequence->lightingScheme[i].duration)
        {
            #ifdef debug
                Serial.println("Begin WhileLoop Iteration Start");
            #endif
            for (uint8_t j = 0; j < lightingSequence->lightingScheme[i].length; ++j)
            {
                #ifdef debug
                    Serial.println("Begin Loop 2 Iteration Start");
                    Serial.println(lightingSequence->lightingScheme[i].length);
                #endif
                
                digitalWrite(lightingSequence->lightingScheme[i].lightingPointArrayRepresentation[j].LAYER, LOW);
                digitalWrite(lightingSequence->lightingScheme[i].lightingPointArrayRepresentation[j].LED, HIGH);
                delayMicroseconds(250);
                digitalWrite(lightingSequence->lightingScheme[i].lightingPointArrayRepresentation[j].LED, LOW);
                digitalWrite(lightingSequence->lightingScheme[i].lightingPointArrayRepresentation[j].LAYER, HIGH);
            }
        }
    }
    #ifdef debug
        Serial.println("endIlluminate()");
    #endif
}

void LightingState::setLightingSequence(LightingSequencePoint* lightingScheme, uint8_t length)
{
    this->lightingSequence = new LightingSequence();
    #ifdef debug
        Serial.println("SetLightingSequence Length");
        Serial.println(length);
    #endif
    this->lightingSequence->lightingScheme = lightingScheme;
    this->lightingSequence->length = length;
    #ifdef debug
        Serial.println("SetLightingSequence Length Result");
        Serial.println(this->lightingSequence->length);             //WHY DIFFERENT???
        uint8_t len = this->lightingSequence->length;
        Serial.println(len);
        int len2 = (int)this->lightingSequence->length;
        Serial.println(len2);
    #endif
}

void LightingState::setLightingSequence(LightingSequence* lightingSequence)
{
    this->lightingSequence = lightingSequence;
}

LightingState::LightingSequence* LightingState::getLightingSequence()
{
    return lightingSequence;
}

LightingState::~LightingState()
{
    delete(lightingSequence);
}

void LightingState::resetLighting()
{
    for (uint8_t i = 0; i < 16; ++i)
    {
        digitalWrite(this->LED[i], LOW);
    }
    for (uint8_t i = 0; i < 4; ++i)
    {
        digitalWrite(this->LAYER[i], HIGH);
    }
}