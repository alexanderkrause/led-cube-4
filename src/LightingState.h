#ifndef LIGHTINGSTATE_H
#define LIGHTINGSTATE_H

#include <Arduino.h>

/*
* Virtual Class as Template for the different lightung states
*/

class LightingState
{
    friend class Lighting;
private:

   
public:
    ~LightingState();
    struct LightingPointArrayRepresentation
    {
        uint8_t LED = 0;
        uint8_t LAYER = 0;
    };

    struct LightingSequencePoint 
    {
        uint16_t duration = 0;      // time in ms
        uint8_t length = 0;
        LightingPointArrayRepresentation* lightingPointArrayRepresentation;
    };

    struct LightingSequence
    {
        LightingSequencePoint* lightingScheme;
        uint8_t length;
    };


    virtual String toString() = 0;
    
    LightingSequencePoint transform(uint8_t* points[], uint8_t length, uint16_t duration);
    

    void setLightingSequence(LightingSequencePoint* lightingScheme, uint8_t length);
    void setLightingSequence(LightingSequence* lightingSequence);
    LightingSequence* getLightingSequence();
    void resetLighting();
    
protected:
    uint8_t* LED;    
    uint8_t* LAYER; 

    LightingSequence* lightingSequence;
    void illuminate();
    virtual void shine(uint32_t argument) = 0;
    virtual void shine() = 0;
};

#endif