#include "Lighting.h"
#include <Arduino.h>


Lighting::Lighting(/* args */)
{
    #ifdef debug
        Serial.println("Lighting created!");
    #endif
}

Lighting::~Lighting()
{
    
}
/*
void Lighting::setLighting(LightingState* state, uint32_t argument)
{
    _state = state;
    #ifdef debug
        Serial.println("State set!");
        Serial.println(_state->toString());
    #endif
    this->shine(argument);
}
*/
void Lighting::setLighting(LightingState* state)
{
    _state = state;
    #ifdef debug
        Serial.println("State set!");
        Serial.println(_state->toString());
    #endif
    //this->shine();
}

void Lighting::shine(uint32_t argument)
{  
    _state->resetLighting();
    _state->shine(argument);         
}

void Lighting::shine()
{  
    _state->resetLighting();
    _state->shine();         
}

void Lighting::resetLighting()
{
    _state->resetLighting();
}